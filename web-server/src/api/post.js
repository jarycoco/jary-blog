import request from '@/utils/request'

export async function fetchPost(param, query) {
    return (await request({
        url: '/post/' + param,
        method: 'get',
        params: query
    })).data
}

export async function fetchPostList(param, query) {
    return (await request({
        url: '/post/list/' + param,
        method: 'get',
        params: query
    })).data
}

export async function fetchPostAllListLatest() {
    return (await request({
        url: '/post/list/latest/all',
        method: 'get'
    })).data
}

export async function fetchPostListLatest(param) {
    return (await request({
        url: '/post/list/latest/' + param,
        method: 'get'
    })).data
}

export async function fetchPostAnother(param) {
    return (await request({
        url: '/post/another/' + param,
        method: 'get'
    })).data
}

export async function createPost(data) {
    return (await request({
        url: '/post/create',
        method: 'post',
        data
    })).data
}

export async function fetchRelationList(data) {
    return (await request({
        url: '/relation/list',
        method: 'get',
        params:data
    })).data
}

export async function createRelation(data) {
    return (await request({
        url: '/relation',
        method: 'post',
        data
    })).data
}

export async function fetchTag(data) {
    return (await request({
        url: '/tag',
        method: 'get',
        data
    })).data
}

export async function createTag(data) {
    return (await request({
        url: '/tag',
        method: 'post',
        data
    })).data
}
