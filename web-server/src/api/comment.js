import request from '@/utils/request'

export async function fetchCommentParentList(param,query) {
  return (await request({
    url: '/comment/parent/list/'+param,
    method: 'get',
    params:query
  })).data
}

export async function fetchCommentChildList(param,query) {
  return (await request({
    url: '/comment/child/list/'+param,
    method: 'get',
    params:query
  })).data
}

export async function createComment(data) {
  return (await request({
      url: '/comment/create',
      method: 'post',
      data
  })).data
}