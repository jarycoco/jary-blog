import request from '@/utils/request'

export async function fetchBoard(param,query) {
  return (await request({
    url: '/board/'+param,
    method: 'get',
  })).data
}

export async function fetchBoardList(param,query) {
  return (await request({
    url: '/board/list/',
    method: 'get',
    params:query
  })).data
}
