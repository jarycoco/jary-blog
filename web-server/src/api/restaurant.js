import request from '@/utils/request'

export async function fetchRestaurant(param) {
  return (await request({
    url: '/restaurant/list/'+param,
    method: 'get'
  })).data
}
