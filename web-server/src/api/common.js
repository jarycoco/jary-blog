
export function dateFormat(date) {
    var d = new Date(date);
    return `${d.getFullYear()}.${numberFormat(d.getMonth() + 1)}.${numberFormat(d.getDate())}`
}

export function timeFormat(date) {
    var d = new Date(date);
    return `${numberFormat(d.getHours())}:${numberFormat(d.getMinutes())}`
}

export function numberFormat(number) {
    return number >= 10 ? number : '0' + number
}

export function transferKor(eng) {
    switch (eng) {
        case 'IT':
            return 'IT 정보'
        case 'software':
            return '소프트웨어'
        case 'algorithm':
            return '알고리즘'
        case 'database':
            return '데이터베이스'
        case 'japan':
            return '일본어'
        default:
            return eng;
    }
}
