import request from '@/utils/request'

export async function fetchCategoryList(param,query) {
  return (await request({
    url: '/category/list/',
    method: 'get',
    params:query
  })).data
}
