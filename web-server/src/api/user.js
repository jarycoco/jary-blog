import request from '@/utils/request'

export async function createUser(data) {
  return (await request({
    url: '/user',
    method: 'post',
    data:data
  }))
}
