// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import { store } from './store'
import App from './App'
import router from './router'
import './element'
import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'tiny-slider/dist/tiny-slider.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import VueQuillEditor from 'vue-quill-editor'
import VueSlideUpDown from 'vue-slide-up-down'

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

library.add(faThumbsUp)
 
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('vue-slide-up-down', VueSlideUpDown)

Vue.config.productionTip = false

Vue.use(Element)
Vue.use(VueQuillEditor, /* { default global options } */)

Vue.prototype.$bus = new Vue();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
