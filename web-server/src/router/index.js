import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/home'
import Post from '@/components/post'
import PostWrite from '@/components/PostWrite'
import IT from '@/views/it'
import Japan from '@/views/japan'
// import JapanPost from '@/views/japan/Post'
import JapanWrite from '@/views/japan/Write'
import Programming from '@/views/programming'
import Layout from '@/views/layout/Layout'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      component: Layout,
      redirect: 'home',
      children: [
        {
          path: 'home',
          components: { Main: Home },
          name: 'japan',
          meta:{title:'all'}
        },
        {
          path: 'post/:id',
          components: { Main: Post },
          name: 'post'
        },
        {
          path: ':category/write',
          components: { Main: PostWrite },
          name: '쓰기'
        }
      ]
    },
    {
      path: '/IT',
      component: Layout,
      redirect: '/',
      children: [
        {
          path: '/',
          components: { Main: IT },
          name: 'it',
          meta:{title:'IT 정보'}
        }
      ]
    },
    {
      path: '/software',
      component: Layout,
      redirect: '/',
      children: [
        {
          path: '/',
          components: { Main: IT },
          name: '소프트웨어',
          meta:{title:'소프트웨어'}
        },
      ]
    },
    {
      path: '/japan',
      component: Layout,
      redirect: '/',
      children: [
        {
          path: '/',
          components: { Main: Japan },
          name: 'japan',
          meta:{title:'일본어'}
        }
      ]
    },
    {
      path: '/programming',
      component: Layout,
      redirect: '/',
      children: [
        {
          path: '/',
          components: { Main: Programming },
          name: 'japan',
          meta:{title:'일본어'}
        },
      ]
    },
    {
      path: '/algorithm',
      component: Layout,
      redirect: '/',
      children: [
        {
          path: '/',
          components: { Main: Programming },
          name: 'algorithm',
          meta:{title:'알고리즘'}
        },
      ]
    },
    {
      path: '/C_C++',
      component: Layout,
      redirect: '/',
      children: [
        {
          path: '/',
          components: { Main: Programming },
          name: 'C C++',
          meta:{title:'C C++'}
        },
      ]
    },
    {
      path: '/nodejs',
      component: Layout,
      redirect: '/',
      children: [
        {
          path: '/',
          components: { Main: Programming },
          name: 'NodeJS',
          meta:{title:'NodeJS'}
        },
      ]
    },
    {
      path: '/database',
      component: Layout,
      redirect: '/',
      children: [
        {
          path: '/',
          components: { Main: Programming },
          name: 'database',
          meta:{title:'데이터베이스'}
        },
      ]
    }
  ]
})
