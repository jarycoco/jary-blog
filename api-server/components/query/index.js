var mysql = require('mysql');
var { format } = require('mysql');
var db_conf = require('../../config/db.conf')
var crypto = require('crypto');

module.exports = {
    execQuery(query) {
        return new Promise(function (resolve, reject) {
            var connection = mysql.createConnection(db_conf);
            connection.connect();
            connection.query(query, function (error, results, fields) {
                connection.end();
                if (error) {
                    console.log(query)
                    // throw error
                    reject(error);
                }
                // console.log(query)
                resolve(results)
            });
        })
    },

    filterToWhere(filter) {
        let query = ''
        if (Object.keys(filter).length > 0) {
            let where = format(` WHERE ?`, filter)
            query += where.replace(/,/g, ' and')
        }
        return query
    },
    format

}
