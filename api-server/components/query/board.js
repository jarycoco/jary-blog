var { execQuery } = require('./index')

const SELECT = 'SELECT * FROM board '

module.exports = {
    async get(name) {
        let data = await execQuery(SELECT + `WHERE name = '${name}'`)
        return data[0]
    },
    async getList() {
        let data = await execQuery(SELECT)
        return data
    }
}