var { execQuery } = require('./index')

const SELECT = 'SELECT * FROM category'

module.exports = {
    async get(id) {
        let data = await execQuery(SELECT)
        return data
    },
    async getList() {
        let data = await execQuery(SELECT)
        return data
    }
}