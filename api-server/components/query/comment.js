var { execQuery,format } = require('./index')

const SELECT = 'SELECT * FROM comment'
const INSERT = 'INSERT INTO comment SET ?'

module.exports = {
    async getParentList(article_id) {
        let query = SELECT + ` WHERE isNULL(comment_id) and post_id = ${article_id}`
        let data = await execQuery(query)
        return data
    },
    async getChildList(parent_comment_id) {
        let query = SELECT + ` WHERE comment_id = ${parent_comment_id}`
        let data = await execQuery(query)
        return data
    },
    async create(data) {
        let query = format(INSERT,data)
        console.log(query)
        let result = await execQuery(query)
        return result
    },
}