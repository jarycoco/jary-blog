var { execQuery, format } = require('./index')

// const SELECT = 'SELECT post.*,count(like.id) as `like`,count(comment.id) as `comment`,user.nickname as author,badge.name as badge_name, badge.color as badge_color FROM post INNER JOIN user ON post.user_id = user.id LEFT JOIN `like` ON post.id = like.post_id LEFT JOIN `comment` ON post.id = comment.post_id LEFT JOIN badge ON post.badge_id = badge.id'
const SELECT = 'SELECT post.* from post'
const INSERT = 'INSERT INTO post SET ?'
module.exports = {
    async get(id) {
        let query = SELECT + ` WHERE post.id = ${id}`
        let data = await execQuery(query)
        return data[0]
    },
    async getList(category, page) {
        if (page) page--;
        else page = 0;
        let query = SELECT
        if (category != 'all') query += ` WHERE post.category = '${category}'`
        query += ` GROUP BY post.id order by date DESC LIMIT 20 OFFSET ${page * 20}`
        let data = await execQuery(query)
        return data
    },
    async getListLatest(category) {
        let query = SELECT + ` WHERE post.category like '${category}'`
        query += ` GROUP BY post.id order by date DESC LIMIT 10`
        console.log(query)
        let data = await execQuery(query)
        return data
    },
    async getAnother(post_id) {
        let post = await this.get(post_id)
        let query = `(${SELECT} WHERE category='${post.category}' and id < ${post_id} ORDER BY id DESC LIMIT 2) UNION (${SELECT} WHERE category='${post.category}' and id > ${post_id} LIMIT 2) `
        let data = await execQuery(query)
        return data
    },
    async create(data) {
        let query = format(INSERT, data)
        // console.log(query)
        let result = await execQuery(query)
        return result
    },
}