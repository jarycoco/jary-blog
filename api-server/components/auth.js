/**
* auth.js
*/

'use strict';

var jwt = require('jsonwebtoken');
var compose = require('composable-middleware');
var SECRET = 'token_secret';
var EXPIRES = 60; // 1 hour

// JWT 토큰 생성 함수
function signToken(data) {
  data.level = 1
  var token = jwt.sign(data, SECRET, { expiresIn: '1h' })
  return token
}

function decodeToken(token) {
  var decoded = jwt.verify(token, SECRET);
  return decoded

}

function login(req, res, next) {
  const token = req.cookies["x-access-token"]
  if(token) req.user = decodeToken(token)
  next()
}
function isAuthenticated(req, res, next) {

  if (req.user.level == 1 || req.user.level == 25) next()
  else res.status(401).end()

}

exports.signToken = signToken;
exports.decodeToken = decodeToken;
exports.isAuthenticated = isAuthenticated;
exports.login = login;