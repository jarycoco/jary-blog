var mysql = require('mysql');
var { format } = require('mysql');
var db_conf = require('../config/db.conf')
var crypto = require('crypto');

/**
 * 
 * @param {string} query 
 */
function execQuery(query) {
    return new Promise(function (resolve, reject) {
        var connection = mysql.createConnection(db_conf);
        connection.connect();
        connection.query(query, function (error, results, fields) {
            connection.end();
            if (error) {
                console.log(query)
                // throw error
                reject(error);
            }
            console.log(query)
            resolve(results)
        });
    })
}

// 필터 오브젝트를 쿼리 WHERE절에 맞게 변환
function filterToWhere(filter){
    let query =''
    if (Object.keys(filter).length > 0) {
        let where = format(` WHERE ?`, filter)
        query += where.replace(/,/g, ' and')
    }
    return query
}

// 쿼리에서 리턴된 값중 키값을 서브 테이블에 맞게 수정
function subTable(data){
    let table = ''
    for (var key in data) {
        if(key.includes('__sub__')){
            table = data[key]
        }
        key = table + '_'+key
    }
    console.log(data)
}

module.exports.execQuery = execQuery

module.exports.board = class board {
    static async get(filter) {
        let query = 'SELECT post.*,count(like.id) as `like`,count(comment.id) as `comment`,user.nickname as nickname,badge.name as badge_name, badge.color as badge_color FROM post INNER JOIN user ON post.user_id = user.id LEFT JOIN `like` ON post.id = like.post_id LEFT JOIN `comment` ON post.id = comment.post_id LEFT JOIN badge ON post.badge_id = badge.id'
        let page = 0
        console.log(filter)
        if (Object.keys(filter).length > 0) {
            if (filter.hasOwnProperty('page')) {
                page = filter['page'] - 1
                delete filter['page']
            }
            let where = format(` WHERE ?`, filter)
            query += where.replace(/,/g, ' and')
        }
        query += ` group by post.id order by date DESC LIMIT 20 OFFSET ${page * 20}`
        // query = query.replace(/,/g, ' and')
        console.log(query)
        var data = await execQuery(query)
        return data
    }
    static async next(id, type) {
        let query = `SELECT * FROM post WHERE type='${type}' and id>${id} ORDER BY id limit 1;`
        // query = query.replace(/,/g, ' and')
        console.log(query)
        var data = await execQuery(query)
        return data
    }
    static async before(id, type) {
        let query = `SELECT * FROM post WHERE type='${type}' and id<${id} ORDER BY id DESC limit 1;`
        // query = query.replace(/,/g, ' and')
        console.log(query)
        var data = await execQuery(query)
        return data
    }
}

module.exports.comment = class comment {
    static async get(filter) {
        let query = `SELECT comment.*,user.nickname as nickname FROM comment INNER JOIN user ON comment.user_id = user.id`
        if (Object.keys(filter).length > 0) {
            let where = format(` WHERE ?`, filter)
            query += where.replace(/,/g, ' and')
        }
        query += " and isNull(`comment_id`)"
        var data = await execQuery(query)
        // let comment = {}

        // 대댓글을 쓰기 좋은 오브젝트로 만들기위한 노오력
        // for (const item of data) {
        //     if (item.comment_id) {
        //         comment[item.comment_id]['child'].push(item)
        //     }
        //     else {
        //         comment[item.id] = item
        //         comment[item.id]['child'] = []
        //     }
        // }
        return data
    }
    static async getChild(comment_id){
        let query = `SELECT comment.*,user.nickname as nickname FROM comment INNER JOIN user ON comment.user_id = user.id WHERE comment_id=${comment_id}`
        var data = await execQuery(query)
        return data
    }
    static async post(obj) {
        let query = format('INSERT INTO `comment` SET ?', obj)
        let data = await execQuery(query)
        return data
    }
}

module.exports.badge = class badge {
    static async get(filter) {
        let query = `SELECT badge.*,board.name as board_name FROM badge INNER JOIN board ON badge.board_id = board.id`
        query += filterToWhere(filter)
        var data = await execQuery(query)
        // subTable(data)
        return data
    }
}

module.exports.user = class user {
    static async login(username, password) {
        const hashPassword = crypto.createHmac('sha256', 'jarycoco').update(password).digest('hex');
        let query = format(`SELECT * FROM user WHERE username = '${username}' and password = '${hashPassword}'`)
        let data = await execQuery(query)
        if (data.length) {
            delete data[0].password
            delete data[0].code
            delete data[0].vertification
        }
        return data
    }
}

module.exports.format = mysql.format;
