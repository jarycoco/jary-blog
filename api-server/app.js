var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var common = require('./routes/common');
var board = require('./routes/board');
var post = require('./routes/post');
var category = require('./routes/category');
var comment = require('./routes/comment');
var relation = require('./routes/relation');
const authRouter = require('./routes/auth');
const user = require('./routes/user');
var auth = require('./components/auth');

var app = express();

const PATH_PUBLIC_IMG = path.resolve(__dirname, 'public/img')
const PATH_WEB = path.resolve(__dirname, '../web-server/dist')

console.log(PATH_WEB)
// view engine setup
app.set('view engine', 'pug');
app.enable('trust proxy');

app.all('/*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  // res.header('Access-Control-Allow-Credentials', true);
  res.header("Access-Control-Allow-Methods", 'POST, GET, PUT, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", 'Content-Type');
  next();
});

app.use(logger('common'));
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(PATH_WEB));
app.use(express.static(PATH_PUBLIC_IMG));

console.log(PATH_PUBLIC_IMG)

// 인증 체크
app.use(auth.login)

app.use('/api/auth', authRouter);
app.use('/api/user', user);

// // 인증 체크
// app.use(auth.isAuthenticated)

app.use(function(req,res,next){
  // 사용자 아이디가 필요한 데이터는 자기 자신으로 바꿔준다
  if(req.body.hasOwnProperty('user_id')){
    req.body.user_id = req.user.id
  }
  next()
})

app.use('/api/post',post)
app.use('/api/board', board);
app.use('/api/category', category);
app.use('/api/comment', comment);
app.use('/api/relation', relation);

app.use('/api/relation', common('relation'));
app.use('/api/tag', common('tag'));
// app.use('/api/category', common('category'));
// app.use('/api/board', common('board'));
// app.use('/api/badge', common('badge'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  if(req.headers.accept.includes('image')){
    res.sendFile(__dirname+'/public/img/none.jpg')
  }
  else next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

process.on('uncaughtException', function (err) {
  console.log('Caught exception: ' + err);
});

module.exports = app;
