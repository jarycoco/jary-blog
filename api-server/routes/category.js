var express = require('express');
var category = require('../components/query/category')
var router = express.Router();

router.get('/list', async function (req, res) {
    let data = await category.getList()
    res.json(data)
});

module.exports = router;
