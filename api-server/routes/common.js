var express = require('express');
var { execQuery, format } = require('../components/query')

function common(table) {
    var router = express.Router();

    router.route('/')
        .get(async (req, res) => {
            var query
            if (Object.keys(req.query).length == 0) {
                query = format(`SELECT * FROM \`${table}\``, req.query)
            }
            else {
                query = format(`SELECT * FROM \`${table}\` WHERE ?`, req.query)
            }
            query = query.replace(/,/g, ' and')
            console.log(query)
            var data = await execQuery(query)
            res.json(data)
        })
        .post(async (req, res) => {
            console.log(req.body)
            var query = format(`INSERT INTO \`${table}\` SET ?`, req.body)
            console.log(query)
            var data = await execQuery(query)
            res.json(data)
        })
        .put(async (req, res) => {
            var key = req.body.key
            delete req.body.key
            var query = format(`UPDATE \`${table}\` SET ? WHERE ${key}='${req.body[key]}'`, req.body)
            console.log(query)
            var data = await execQuery(query)
            res.json(data)
        })
        .delete(async (req, res) => {
            var query = format(`DELETE FROM \`${table}\` WHERE ?`, req.body)
            query = query.replace(/,/g, ' and')
            console.log(query)
            var data = await execQuery(query)
            res.json(data)
        })

    return router
}

module.exports = common;
