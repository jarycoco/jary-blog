var express = require('express');
var comment = require('../components/query/comment')
var router = express.Router();

router.get('/parent/list/:article_id', async function (req, res) {
    let data = await comment.getParentList(req.params.article_id)
    res.json(data)
});

router.get('/child/list/:parent_comment_id', async function (req, res) {
    let data = await comment.getChildList(req.params.parent_comment_id)
    res.json(data)
});

router.post('/create', async (req, res) => {
    console.log(req.body)
    let result = await comment.create(req.body)
    res.json(result)
})

module.exports = router;
