var express = require('express');
var relation = require('../components/query/relation')
var router = express.Router();

router.get('/list', async function (req, res) {
    let data = await relation.getList(req.query.post_id)
    res.json(data)
});

module.exports = router;
