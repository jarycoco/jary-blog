var express = require('express');
var router = express.Router();
var img = require('../jary-library/image')
var path = require('path')
var { execQuery, format, board, comment,badge } = require('../components/query')
var {isAuthenticated} = require('../components/auth')

const PATH_PUBLIC_IMG = path.resolve(__dirname, '../public/img')

router.get('/post', async (req, res) => {
    let result = await board.get(req.query)
    res.json(result)
})
router.get('/post/next',isAuthenticated, async (req, res) => {
    let result = await board.next(req.query.id,req.query.type)
    res.json(result)
})
router.get('/post/before', async (req, res) => {
    let result = await board.before(req.query.id,req.query.type)
    res.json(result)
})

router.post('/post', async (req, res, next) => {
    console.log(req.body)
    var tags = img.parseImgTag(req.body.content)
    for (const index in tags) {
        var tag = tags[index]
        var base64 = tag[1]
        var filename = tag[2].split('.')[0]
        var image = ''
        if (index == 0) {
            image = await img.base64toThumbnail(base64, PATH_PUBLIC_IMG, filename)
            req.body.thumbnail = image.thumbnail
        }
        else {
            image = await img.base64toImg(base64, PATH_PUBLIC_IMG, filename)
        }
        req.body.content = req.body.content.replace(base64, 'http://jary.hopto.org:7011/' + image.name)
        console.log(req.body.content)
    }
    next()
})

router.get('/comment', async (req, res) => {
    let result = await comment.get(req.query)
    res.json(result)
})

router.get('/comment/child', async (req, res) => {
    let result = await comment.getChild(req.query.comment_id)
    res.json(result)
})

router.post('/comment', async (req, res) => {
    req.body.user_id = req.user.id
    let result = await comment.post(req.body)
    res.json(result)
})

router.get('/badge', async (req, res) => {
    let result = await badge.get(req.query)
    res.json(result)
})

module.exports = router;
