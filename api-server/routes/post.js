var express = require('express');
var post = require('../components/query/post')
var img = require('../jary-library/image')
var path = require('path')
var router = express.Router();
const uuidv4 = require('uuid/v4');
const PATH_PUBLIC_IMG = path.resolve(__dirname, '../public/img')

router.get('/:id', async function (req, res) {
    let data = await post.get(req.params.id)
    res.json(data)
});

router.get('/list/:type', async function (req, res) {
    let data = await post.getList(req.params.type,req.query.page)
    res.json(data)
});

router.get('/list/latest/all', async function (req, res) {
    console.log('asf')
    let data = await post.getListLatest('%')
    res.json(data)
});

router.get('/list/latest/:type', async function (req, res) {
    let data = await post.getListLatest(req.params.type)
    res.json(data)
});

router.get('/another/:id', async function (req, res) {
    let data = await post.getAnother(req.params.id)
    res.json(data)
});

router.post('/create', async (req, res) => {
    // console.log(req.body)

    //console.log(req.body.content)
    var tags = img.parseImgTag(req.body.content)

    // console.log(tags)
    for (const index in tags) {
        var tag = tags[index]
        var base64 = tag[1]
        var filename = uuidv4()
        var image = ''
        var image2 = ''
        if (index == 0) {
            console.log('asf')
            image = await img.base64toThumbnail(base64, PATH_PUBLIC_IMG, filename,200,200)
            image2 = await img.base64toThumbnail(base64, PATH_PUBLIC_IMG, filename,1920,400)
            req.body.thumbnail = image.thumbnail
            req.body.thumbnail2 = image2.thumbnail
            console.log(req.body)
        }
        else {
            image = await img.base64toImg(base64, PATH_PUBLIC_IMG, filename)
        }
        req.body.content = req.body.content.replace(base64, '/' + image.name)
        // console.log(req.body.content)
    }
    let result = await post.create(req.body)
    res.json(result)
})

module.exports = router;
