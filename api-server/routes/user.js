var express = require('express');
var user = require('../components/query/user')
var {sendMail} = require('../jary-library/mail')
var router = express.Router();
const crypto = require('crypto');

const salt = 'Jary1234'

function getRandomIntInclusive(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

router.post('/', async function (req, res) {
    crypto.pbkdf2(req.body.password, salt, 100000, 64, 'sha512', async (err, key) => {
        req.body.password = key.toString('base64')
        let code = getRandomIntInclusive(123456,987654)
        req.body.code = code
        sendMail(req.body.email,code)
        let data = await user.create(req.body)
        res.json(data)
      });
});
module.exports = router;
