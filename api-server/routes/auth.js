var express = require('express');
var auth = require('../components/auth');
var user = require('../components/query/user')
var router = express.Router();
const crypto = require('crypto');

const salt = 'Jary1234'

router.post('/signup', (req, res, next) => {
    res.send('ok');
});

router.post('/login', async function (req, res, next) {
    console.log(req.body)

    crypto.pbkdf2(req.body.password, salt, 100000, 64, 'sha512', async (err, key) => {
        req.body.password = key.toString('base64')
        let u = await user.get(req.body.username,req.body.password)
        var token=""
        console.log(u)
        if (u) {
            token = auth.signToken(JSON.parse(JSON.stringify(u)))
            console.log(token)
        }
        res.cookie('x-access-token',token, { maxAge: 900000, httpOnly: true })
        res.setHeader('x-access-token',token)
        res.json(token)
    
      });

});

router.get('/logout', function(req, res) {
    res.cookie('x-access-token','', { maxAge: 900000, httpOnly: true })
    res.status(200).end();
  });

router.post('/vertification', async function (req, res) {
    let result = await user.vertification(req.user.username,req.body.code)
    let success = result.changedRows ? 'success' : 'fail'
    res.json({
        result:success
    })
});

module.exports = router;
