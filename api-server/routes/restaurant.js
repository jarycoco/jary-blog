var express = require('express');
var restaurant = require('../components/query/restaurant')
var router = express.Router();

router.get('/list/:type', async function (req, res) {
    let data = await restaurant.getList(req.params.type)
    res.json(data)
});

module.exports = router;
