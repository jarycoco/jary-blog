var express = require('express');
var board = require('../components/query/board')
var router = express.Router();

router.get('/list', async function (req, res) {
    let data = await board.getList()
    res.json(data)
});

router.get('/:name', async function (req, res) {
    let data = await board.get(req.params.name)
    res.json(data)
});
module.exports = router;
